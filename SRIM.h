//
// C++ class to read the output of a SRIM calculation.
// Dependencies: C++11 & ROOT
//
// Author: Ryan M.L. McFadden
//

#ifndef SRIM_H
#define SRIM_H

// C++ headers
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

// ROOT headers
#include <Math/DistFunc.h>
#include <TF1.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TTree.h>

class SRIM {
public:
  // Constructors:
  SRIM(std::string RANGE_filename);
  SRIM(std::string RANGE_filename, std::string RANGE_3D_filename);

  // Methods to access private data members:
  double GetIonsSimulated();
  double GetIonsImplanted();
  double GetIonsTransmitted();
  double GetIonsBackscattered();

  double GetFractionBackscattered();
  double GetFractionTransmitted();

  std::string GetVersion();
  std::string GetIon();
  // for RANGE.txt
  double GetRange();
  double GetStraggle();
  double GetRadialRange();
  double GetRadialStraggle();
  double GetLateralRange();
  double GetLateralStraggle();
  // for RANGE_3D.txt
  double GetRangeX();
  double GetStraggleX();
  double GetRangeY();
  double GetStraggleY();
  double GetRangeZ();
  double GetStraggleZ();

  double GetIntegralX(double a, double b);
  double GetIntegralY(double a, double b);
  double GetIntegralZ(double a, double b);

  double GetZ();
  double GetEnergy();
  double GetMass();
  double GetAngle();

  // Histograms of the ion stopping distribution:
  // for RANGE.txt
  TGraph gRANGE;
  // for RANGE_3D.txt.
  TH1D hRANGE_3D_X;
  TH1D hRANGE_3D_Y;
  TH1D hRANGE_3D_Z;
  TH3D hRANGE_3D;

  // PDF functions to paramaterize the ion stopping distribution:
  TF1 fGaussian;
  TF1 fTruncatedGaussian;
  TF1 fGamma;
  TF1 fBeta;

  // TTrees
  // TTree Profile;
  // TTree Profile3D;

  // Print data members to terminal
  void GetSummary();

  // Save histogram to ascii
  void SaveASCII(const std::string directory);

private:
  // Filenames for RANGE.txt and RANGE_3D.txt to be read.
  std::string RANGE_filename;
  std::string RANGE_3D_filename;

  // Private methods for reading RANGE.txt and RANGE_3D.txt.
  void RANGE_Read();
  void RANGE_3D_Read();

  // Private method to intialize the PDFs.
  void PDFs();

  // Convert Angstroms (SRIM's default output) to nm.
  const double A_to_nm = 10.0;

  // Flags indicating if next lines in RANGE.txt and RANGE_3D.txt are stopping
  // distribution tables.
  bool RANGE_ReadDistribution;
  bool RANGE_3D_ReadDistribution;

  // Data members:
  unsigned int IonsSimulated;     // total number of ions in simulation
  unsigned int IonsImplanted;     // total number of ions that stop in target
  unsigned int IonsBackscattered; // total number of ions backscattered
  unsigned int IonsTransmitted;   // total number of ions transmitted

  std::string Version;
  std::string Ion;
  double FractionBackscattered;
  double FractionTransmitted;

  double Energy; // keV
  unsigned int Z;
  double Mass;  // amu
  double Angle; // degrees

  double Range;
  double Straggle;
  double RadialRange;
  double RadialStraggle;
  double LateralRange;
  double LateralStraggle;

  double RangeX;
  double StraggleX;
  double RangeY;
  double StraggleY;
  double RangeZ;
  double StraggleZ;

  // helper member function for doing the integration
  double GetIntegral(const TH1D &h, double a, double b);

  // Identifying strings.
  const std::string sVersion = "SRIM-";
  const std::string sIons = "Ion    =";
  const std::string sIonsEnergy = "Energy =";
  const std::string sIonsSimulated = "Total Ions calculated =";
  const std::string sIonsAverageRange = "Ion Average Range =";
  const std::string sIonsLateralRange = "Ion Lateral Range =";
  const std::string sIonsRadialRange = "Ion Radial  Range =";
  const std::string sIonsStraggling = "Straggling =";
  const std::string sIonsTransmitted = "Transmitted Ions =";
  const std::string sIonsBackscattered = "Backscattered Ions =";
  const std::string sIonsAngle = "Ion Angle to Surface =";
  const std::string sIons3D = "Ion =";
  const std::string sIonsMass = "Ion Mass=";

  // Vectors for RANGE.txt output.
  std::vector<double> D_RANGE; // Depth (nm)
  std::vector<double> I_RANGE; // Ions [(Atoms/cm3) / (Atoms/cm2)]
  std::vector<double> P_RANGE; // Ions normalized
  std::vector<double> T_RANGE; // Target Atoms [(Atoms/cm3) / (Atoms/cm2)]

  // Vectors for RANGE_3D.txt output.
  std::vector<double> N_RANGE_3D; // Ion Number
  std::vector<double> X_RANGE_3D; // X (nm) <--- Implantation depth
  std::vector<double> Y_RANGE_3D; // Y (nm)
  std::vector<double> Z_RANGE_3D; // Z (nm)
};

// Read RANGE.txt files.
void SRIM::RANGE_Read() {
  std::ifstream RANGE_file(RANGE_filename);
  if (RANGE_file.is_open()) {
    std::string line;

    while (std::getline(RANGE_file, line)) {
      // Extract SRIM version.
      if (line.find(sVersion) != std::string::npos) {
        Version.append(line);
        Version.erase(std::remove(Version.begin(), Version.end(), ' '),
                      Version.end());
      }

      // Extract ion and implantation energy.
      if (line.find(sIons) != std::string::npos and
          line.find(sIonsEnergy) != std::string::npos) {
        char tmp_ion[8];
        sscanf(line.c_str(), "  Ion    = %s   Energy = %lf  keV", tmp_ion,
               &Energy);
        Ion.append(tmp_ion);
        // std::cout << "tmp_ion = " << tmp_ion << std::endl;
      }

      // Extract total ions calculated.
      if (line.find(sIonsSimulated) != std::string::npos) {
        sscanf(line.c_str(), " Total Ions calculated =%u", &IonsSimulated);
      }

      // Extract average range and straggle.
      if (line.find(sIonsAverageRange) != std::string::npos and
          line.find(sIonsStraggling) != std::string::npos) {
        sscanf(line.c_str(),
               " Ion Average Range =   %lf A   Straggling =   %lf A", &Range,
               &Straggle);
        Range /= A_to_nm;
        Straggle /= A_to_nm;
      }

      // Extract lateral range and straggle.
      if (line.find(sIonsLateralRange) != std::string::npos and
          line.find(sIonsStraggling) != std::string::npos) {
        sscanf(line.c_str(),
               " Ion Lateral Range =   %lf A   Straggling =   %lf A",
               &LateralRange, &LateralStraggle);
        LateralRange /= A_to_nm;
        LateralStraggle /= A_to_nm;
      }

      // Extract radial range and straggle.
      if (line.find(sIonsRadialRange) != std::string::npos and
          line.find(sIonsStraggling) != std::string::npos) {
        sscanf(line.c_str(),
               " Ion Radial  Range =   %lf A   Straggling =   %lf A",
               &RadialRange, &RadialStraggle);
        RadialRange /= A_to_nm;
        RadialStraggle /= A_to_nm;
      }

      // Extract transmitted and backscattered ions.
      if (line.find(sIonsTransmitted) != std::string::npos and
          line.find(sIonsBackscattered) != std::string::npos) {
        if (line.find("=;") != std::string::npos) {
          sscanf(line.c_str(), " Transmitted Ions =;  Backscattered Ions =%u",
                 &IonsBackscattered);
          IonsTransmitted = 0;
        } else {
          sscanf(line.c_str(), " Transmitted Ions =%u;  Backscattered Ions =%u",
                 &IonsTransmitted, &IonsBackscattered);
        }
      }

      // Extract stopping distribution.
      /*
      if ( RANGE_ReadDistribution == true )
      {
         double depth, ions, tgt;
         sscanf(line.c_str(), "%lf  %lf  %lf", &depth, &ions, &tgt);
         D_RANGE.push_back(depth/A_to_nm);
         I_RANGE.push_back(ions/A_to_nm);
         T_RANGE.push_back(tgt/A_to_nm);
      }
      */

      // Stopping distribution flag.
      if (line.find("-----------  ----------  ------------") !=
          std::string::npos) {
        RANGE_ReadDistribution = true;
        double depth, ions, target;
        // TTree t(RANGE_filename.c_str(), RANGE_filename.c_str());
        // t.Branch("dr",&d,"dr/D");
        // t.Branch("ir",&i,"ir/D");
        // t.Branch("tr",&t,"tr/D");
        line.clear();
        while (std::getline(RANGE_file, line)) {
          std::istringstream iss(line);
          iss >> depth >> ions >> target;
          D_RANGE.push_back(depth / A_to_nm);
          I_RANGE.push_back(ions);
          T_RANGE.push_back(target);
        }
        // t.Clone();
        // Profile.ReadStream(RANGE_file,"X/D:N/D:R/D");
        // Profile.Scan();
      }
      line.clear();
    }
  }
  RANGE_file.close();
  IonsImplanted = IonsSimulated - IonsTransmitted - IonsBackscattered;
  FractionBackscattered = static_cast<double>(IonsBackscattered) /
                          static_cast<double>(IonsSimulated);
  FractionTransmitted =
      static_cast<double>(IonsTransmitted) / static_cast<double>(IonsSimulated);

  // Some normalization of RANGE.txt vectors
  double i_sum = std::accumulate(std::begin(I_RANGE), std::end(I_RANGE), 0.0);
  for (std::size_t i = 0; i < I_RANGE.size(); ++i) {
    P_RANGE.push_back(I_RANGE.at(i) / i_sum);
  }

  // Get the "bin width"
  std::vector<double> diff(D_RANGE.size());
  std::adjacent_difference(D_RANGE.begin(), D_RANGE.end(), &diff.at(0));
  double step = diff.at(3);

  // Graph the RANGE.txt distribution.
  gRANGE = TGraph(D_RANGE.size(), &D_RANGE.at(0), &P_RANGE.at(0));
  // gRANGE.SetName("gRANGE");
  gRANGE.SetTitle("RANGE.txt;Depth (nm);Stopping Probability (nm^{-1})");
  gRANGE.SetMarkerColor(kBlack);
  gRANGE.SetLineColor(kBlack);
  gRANGE.SetMarkerStyle(kFullCircle);
}

// Read RANGE_3D.txt files.
void SRIM::RANGE_3D_Read() {
  std::ifstream RANGE_3D_file(RANGE_3D_filename);
  if (RANGE_3D_file.is_open()) {
    std::string line;

    while (std::getline(RANGE_3D_file, line)) {
      // Extract ion implantation angle relative to surface.
      if (line.find(sIonsAngle) != std::string::npos) {
        sscanf(line.c_str(), "Ion Angle to Surface = %lf degrees", &Angle);
      }

      // Extract ion and ion mass.
      if (line.find(sIons3D) != std::string::npos and
          line.find(sIonsMass) != std::string::npos) {
        char tmp_ion_3D[8];
        sscanf(line.c_str(), "Ion = %s (%u)   Ion Mass= %lf", tmp_ion_3D, &Z,
               &Mass);
        // std::cout << "tmp_ion 3D : " << tmp_ion_3D << std::endl;
      }

      /*
      // Extract stopping distribution.
      if ( RANGE_3D_ReadDistribution == true )
      {
         double n, x, y, z;
         sscanf(line.c_str(), "%lf  %lf  %lf  %lf", &n, &x, &y, &z);
         //std::cout << n << " " << x/A_to_nm << " " << y/A_to_nm << " " <<
      z/A_to_nm << std::endl; N_RANGE_3D.push_back(n);
         X_RANGE_3D.push_back(x/A_to_nm);
         Y_RANGE_3D.push_back(y/A_to_nm);
         Z_RANGE_3D.push_back(z/A_to_nm);
      }
      */

      // Stopping distribution flag.
      if (line.find("-------  ----------- ----------- -----------") !=
          std::string::npos) {
        RANGE_3D_ReadDistribution = true;
        // Profile3D.ReadStream(RANGE_3D_file,"N/I:X/D:Y/D:Z/D");
        // unsigned int N;
        double n, x, y, z;
        // Profile3D.Branch("N",&N,"N/i");
        // Profile3D.Branch("X",&X,"X/D");
        // Profile3D.Branch("Y",&Y,"Y/D");
        // Profile3D.Branch("Z",&Z,"Z/D");
        line.clear();
        while (std::getline(RANGE_3D_file, line)) {
          std::istringstream iss(line);
          iss >> n >> x >> y >> z;
          N_RANGE_3D.push_back(n);
          X_RANGE_3D.push_back(x / A_to_nm);
          Y_RANGE_3D.push_back(y / A_to_nm);
          Z_RANGE_3D.push_back(z / A_to_nm);
        }
      }
      line.clear();
    }
  }
  RANGE_3D_file.close();

  // Histogram the RANGE_3D.txt distribution:

  // Find the max/min values for each dimension, round them up/down, and use
  // then to automatically determine the ideal histogram ranges for 1 nm bins.
  double x_min = *std::min_element(X_RANGE_3D.begin(), X_RANGE_3D.end());
  double x_max = *std::max_element(X_RANGE_3D.begin(), X_RANGE_3D.end());
  double x_min_le = floor(x_min);
  double x_max_ue = ceil(x_max);
  int x_bins = static_cast<int>(x_max_ue) - static_cast<int>(x_min_le);

  double y_min = *std::min_element(Y_RANGE_3D.begin(), Y_RANGE_3D.end());
  double y_max = *std::max_element(Y_RANGE_3D.begin(), Y_RANGE_3D.end());
  double y_min_le = floor(y_min);
  double y_max_ue = ceil(y_max);
  int y_bins = static_cast<int>(y_max_ue) - static_cast<int>(y_min_le);

  double z_min = *std::min_element(Z_RANGE_3D.begin(), Z_RANGE_3D.end());
  double z_max = *std::max_element(Z_RANGE_3D.begin(), Z_RANGE_3D.end());
  double z_min_le = floor(z_min);
  double z_max_ue = ceil(z_max);
  int z_bins = static_cast<int>(z_max_ue) - static_cast<int>(z_min_le);

  // std::cout << x_bins << std::endl;
  // std::cout << floor(x_min) << std::endl;

  // Assign "dynamic" names to histograms in order to avoid TNamed collisions.
  // Is there a better solution to this?
  std::string hname_3D_X("hRANGE_3D_X_");
  hname_3D_X.append(std::to_string(Energy) + "_keV");
  std::string hname_3D_Y("hRANGE_3D_Y_");
  hname_3D_Y.append(std::to_string(Energy) + "_keV");
  std::string hname_3D_Z("hRANGE_3D_Z_");
  hname_3D_Z.append(std::to_string(Energy) + "_keV");
  std::string hname_3D("hRANGE_3D_");
  hname_3D.append(std::to_string(Energy) + "_keV");

  // Initialize the histograms.
  hRANGE_3D_X = TH1D(hname_3D_X.c_str(),
                     "hRANGE_3D_X;X (nm);Stopping Probability (nm^{-1})",
                     x_bins, x_min_le, x_max_ue);
  hRANGE_3D_Y = TH1D(hname_3D_Y.c_str(),
                     "hRANGE_3D_Y;Y (nm);Stopping Probability (nm^{-1})",
                     y_bins, y_min_le, y_max_ue);
  hRANGE_3D_Z = TH1D(hname_3D_Z.c_str(),
                     "hRANGE_3D_Z;Z (nm);Stopping Probability (nm^{-1})",
                     z_bins, z_min_le, z_max_ue);
  hRANGE_3D = TH3D(hname_3D.c_str(), "hRANGE_3D; X (nm); Y (nm); Z (nm)",
                   x_bins, x_min_le, x_max_ue, y_bins, y_min_le, y_max_ue,
                   z_bins, z_min_le, z_max_ue);

  for (std::size_t i = 0; i < N_RANGE_3D.size(); ++i) {
    hRANGE_3D_X.Fill(X_RANGE_3D.at(i));
    hRANGE_3D_Y.Fill(Y_RANGE_3D.at(i));
    hRANGE_3D_Z.Fill(Z_RANGE_3D.at(i));
    hRANGE_3D.Fill(X_RANGE_3D.at(i), Y_RANGE_3D.at(i), Z_RANGE_3D.at(i));
  }

  // Normalize the histograms.
  double Normalization = 1.0 / static_cast<double>(IonsImplanted);
  hRANGE_3D_X.Scale(Normalization);
  hRANGE_3D_Y.Scale(Normalization);
  hRANGE_3D_Z.Scale(Normalization);
  // hRANGE_3D.Scale( Normalization );

  // Compute the range/straggle (i.e., mean/standard deviation) for the x, y,
  // and z directions (using the raw distributions).
  // http://stackoverflow.com/a/12405793

  double x_sum =
      std::accumulate(std::begin(X_RANGE_3D), std::end(X_RANGE_3D), 0.0);
  double x_avg = x_sum / X_RANGE_3D.size();
  double x_accum = 0.0;
  std::for_each(std::begin(X_RANGE_3D), std::end(X_RANGE_3D),
                [&](const double x) { x_accum += (x - x_avg) * (x - x_avg); });

  double y_sum =
      std::accumulate(std::begin(Y_RANGE_3D), std::end(Y_RANGE_3D), 0.0);
  double y_avg = y_sum / Y_RANGE_3D.size();
  double y_accum = 0.0;
  std::for_each(std::begin(Y_RANGE_3D), std::end(Y_RANGE_3D),
                [&](const double y) { y_accum += (y - y_avg) * (y - y_avg); });

  double z_sum =
      std::accumulate(std::begin(Z_RANGE_3D), std::end(Z_RANGE_3D), 0.0);
  double z_avg = z_sum / Z_RANGE_3D.size();
  double z_accum = 0.0;
  std::for_each(std::begin(Z_RANGE_3D), std::end(Z_RANGE_3D),
                [&](const double z) { z_accum += (z - z_avg) * (z - z_avg); });

  double x_stdev = 0.0;
  double y_stdev = 0.0;
  double z_stdev = 0.0;

  // For different definitions of standard deviations
  // - SRIM uses the population definition
  bool is_population = true;
  if (is_population == true) {
    x_stdev = sqrt(x_accum / X_RANGE_3D.size()); // population
    y_stdev = sqrt(y_accum / Y_RANGE_3D.size()); // population
    z_stdev = sqrt(z_accum / Z_RANGE_3D.size()); // population
  } else {
    x_stdev = sqrt(x_accum / (X_RANGE_3D.size() - 1)); // sample
    y_stdev = sqrt(y_accum / (Y_RANGE_3D.size() - 1)); // sample
    z_stdev = sqrt(z_accum / (Z_RANGE_3D.size() - 1)); // sample
  }

  RangeX = x_avg;
  RangeY = y_avg;
  RangeZ = z_avg;

  StraggleX = x_stdev;
  StraggleY = y_stdev;
  StraggleZ = z_stdev;
}

// Initialize the PDFs:
void SRIM::PDFs() {
  fGaussian = TF1("fGaussian", "ROOT::Math::gaussian_pdf(x,[0],[1])", 0, 500);
  // fTrancatedGaussian =
  fBeta = TF1("fBeta", "ROOT::Math::beta_pdf(x/[2],[0],[1])/[2]", 0, 500);
  fBeta.SetParameter(0, 5);
  fBeta.SetParameter(1, 5);
  fBeta.SetParameter(2, 300);
  fBeta.SetParName(0, "k");
  fBeta.SetParName(1, "s");
  fBeta.SetParName(2, "cutoff");

  fGamma = TF1("fGamma", "ROOT::Math::gamma_pdf(x,[0],[1],0)", 0, 500);
  fGamma.SetParameter(0, 5);
  fGamma.SetParameter(1, 5);
  fGamma.SetParName(0, "alpha");
  fGamma.SetParName(1, "theta");

  // std::cout << "pdf for " << RANGE_filename << " and " << RANGE_3D_filename
  // << std::endl;
}

// Constructor for RANGE.txt input.
SRIM::SRIM(std::string RANGE_file)
    : RANGE_filename(RANGE_file), RANGE_ReadDistribution(false),
      RANGE_3D_ReadDistribution(false), IonsSimulated(0), IonsImplanted(0),
      IonsBackscattered(0), IonsTransmitted(0), Energy(0), Z(0), Mass(0),
      Angle(0), Range(0), Straggle(0), RadialRange(0), RadialStraggle(0),
      LateralRange(0), LateralStraggle(0), RangeX(0), StraggleX(0), RangeY(0),
      StraggleY(0), RangeZ(0), StraggleZ(0) {
  // Read the file.
  RANGE_Read();
  // Set up the PDFs.
  PDFs();
}

// Constructor for RANGE.txt and RANGE_3D.txt input.
SRIM::SRIM(std::string RANGE_file, std::string RANGE_3D_file)
    : RANGE_filename(RANGE_file), RANGE_3D_filename(RANGE_3D_file),
      RANGE_ReadDistribution(false), RANGE_3D_ReadDistribution(false),
      IonsSimulated(0), IonsImplanted(0), IonsBackscattered(0),
      IonsTransmitted(0), Energy(0), Z(0), Mass(0), Angle(0), Range(0),
      Straggle(0), RadialRange(0), RadialStraggle(0), LateralRange(0),
      LateralStraggle(0), RangeX(0), StraggleX(0), RangeY(0), StraggleY(0),
      RangeZ(0), StraggleZ(0) {
  // Read the files.
  RANGE_Read();
  RANGE_3D_Read();
  // Set up the PDFs.
  PDFs();
}

void SRIM::SaveASCII(const std::string directory) {
  // Limit Energy to 3 places after the decimal
  std::stringstream ss;
  ss << std::fixed << std::setprecision(3) << Energy;
  //
  if (RANGE_ReadDistribution == true) {
    std::string outname;
    outname.append(directory);
    outname.append("/profile_" + ss.str() + "_keV.dat");
    // std::cout << outname << std::endl;
    // C-style printing, yuck... I'll fix this later.
    FILE *fout = fopen(outname.c_str(), "w+");
    fprintf(fout, "#Depth(nm)\tStoppingProbability(1/nm)\n");

    for (std::size_t i = 0; i < D_RANGE.size(); ++i) {
      fprintf(fout, "% .9f\t% .9f\n", D_RANGE.at(i), P_RANGE.at(i));
    }
    fclose(fout);
  }
  //
  if (RANGE_3D_ReadDistribution == true) {
    std::string outname;
    outname.append(directory);
    outname.append("/profile3D_" + ss.str() + "_keV.dat");
    // std::cout << outname << std::endl;
    // C-style printing, yuck... I'll fix this later.
    FILE *fout = fopen(outname.c_str(), "w+");
    fprintf(fout, "#Depth(nm)\tStoppingProbability(1/nm)\n");
    for (int i = 1; i <= hRANGE_3D_X.GetNbinsX(); i++) {
      fprintf(fout, "% .9f\t% .9f\n", hRANGE_3D_X.GetBinCenter(i),
              hRANGE_3D_X.GetBinContent(i));
    }
    fclose(fout);
  }
  //
  if (RANGE_ReadDistribution == false and RANGE_3D_ReadDistribution == false) {
    std::cout << "Error: no implantation profiles to save." << std::endl;
  }
}

void SRIM::GetSummary() {
  std::cout << " " << std::endl;
  std::cout << "  Version            = " << Version << std::endl;
  std::cout << "  Ion                = " << Ion << std::endl;
  std::cout << "  Z                  = " << Z << std::endl;
  std::cout << "  Mass               = " << Mass << " u" << std::endl;
  std::cout << "  Energy             = " << Energy << " keV" << std::endl;
  std::cout << "  Angle              = " << Angle << " deg" << std::endl;
  std::cout << "  Ions Simulated     = " << IonsSimulated << std::endl;
  std::cout << "  Ions Implanted     = " << IonsImplanted << std::endl;
  std::cout << "  Ions Transmitted   = " << IonsTransmitted << std::endl;
  std::cout << "  Ions Backscattered = " << IonsBackscattered << std::endl;
  if (RANGE_ReadDistribution == true) {
    std::cout << "  Stopping distribution summary for " << RANGE_filename
              << std::endl;
    std::cout << "  Range              = " << Range << " +/- " << Straggle
              << " nm" << std::endl;
    std::cout << "  Lateral Range      = " << LateralRange << " +/- "
              << LateralStraggle << " nm" << std::endl;
    std::cout << "  Radial Range       = " << RadialRange << " +/- "
              << RadialStraggle << " nm" << std::endl;
  }
  if (RANGE_3D_ReadDistribution == true) {
    std::cout << "  3D stopping distribution summary for " << RANGE_3D_filename
              << std::endl;
    std::cout << "  X Range            = " << RangeX << " +/- " << StraggleX
              << " nm" << std::endl;
    std::cout << "  Y Range            = " << RangeY << " +/- " << StraggleY
              << " nm" << std::endl;
    std::cout << "  Z Range            = " << RangeZ << " +/- " << StraggleZ
              << " nm" << std::endl;
  }
  std::cout << " " << std::endl;
}

double SRIM::GetIntegral(const TH1D &h, double a, double b) {
  // swap the bounds for integration if a > b
  if (a > b) {
    std::cout << "\n\tWARNING: swapping integration limits!\n\n";
    std::swap(a, b);
  }

  // ROOT histogram constants
  const auto n_bins = h.GetNbinsX();
  const auto underflow_bin = 0;
  const auto overflow_bin = n_bins + 1;

  // placeholder default values are just the full range
  auto a_index = 1;
  auto b_index = n_bins;

  // check every bin to see which ones a & b fall within
  // including underflow/overflow bins
  for (int i = underflow_bin; i <= overflow_bin; ++i) {
    // find the index for the lower bound of the integration
    if ((a >= h.GetXaxis()->GetBinLowEdge(i)) and
        (a < h.GetXaxis()->GetBinUpEdge(i))) {
      a_index = i;
    }
    // find the index for the upper bound of the integration
    if ((b >= h.GetXaxis()->GetBinLowEdge(i)) and
        (b < h.GetXaxis()->GetBinUpEdge(i))) {
      b_index = i;
    }
  }

  // check if the integration limits are outside of the histogram range
  // lower integration limit
  if (a < h.GetXaxis()->GetBinLowEdge(underflow_bin)) {
    a_index = underflow_bin;
  }
  if (a > h.GetXaxis()->GetBinUpEdge(overflow_bin)) {
    a_index = overflow_bin;
  }
  // upper integration limit
  if (b < h.GetXaxis()->GetBinLowEdge(underflow_bin)) {
    b_index = underflow_bin;
  }
  if (b > h.GetXaxis()->GetBinUpEdge(overflow_bin)) {
    b_index = overflow_bin;
  }

  // return the integral
  return h.Integral(a_index, b_index);
}

double SRIM::GetIntegralX(double a, double b) {
  return SRIM::GetIntegral(hRANGE_3D_X, a, b);
}

double SRIM::GetIntegralY(double a, double b) {
  return SRIM::GetIntegral(hRANGE_3D_Y, a, b);
}

double SRIM::GetIntegralZ(double a, double b) {
  return SRIM::GetIntegral(hRANGE_3D_Z, a, b);
}

// Functions to access private members.
std::string SRIM::GetVersion() { return Version; }
std::string SRIM::GetIon() { return Ion; }

// These are for RANGE.txt values
double SRIM::GetRange() { return Range; }
double SRIM::GetStraggle() { return Straggle; }
double SRIM::GetLateralRange() { return LateralRange; }
double SRIM::GetLateralStraggle() { return LateralStraggle; }
double SRIM::GetRadialRange() { return RadialRange; }
double SRIM::GetRadialStraggle() { return RadialStraggle; }

// These are computed from the RANGE_3D.txt distribution
double SRIM::GetRangeX() { return RangeX; }
double SRIM::GetStraggleX() { return StraggleX; }
double SRIM::GetRangeY() { return RangeY; }
double SRIM::GetStraggleY() { return StraggleY; }
double SRIM::GetRangeZ() { return RangeZ; }
double SRIM::GetStraggleZ() { return StraggleZ; }

double SRIM::GetEnergy() { return Energy; }
double SRIM::GetZ() { return Z; }
double SRIM::GetMass() { return Mass; }
double SRIM::GetAngle() { return Angle; }
double SRIM::GetIonsSimulated() { return IonsSimulated; }
double SRIM::GetIonsImplanted() { return IonsImplanted; }
double SRIM::GetIonsTransmitted() { return IonsTransmitted; }
double SRIM::GetIonsBackscattered() { return IonsBackscattered; }

double SRIM::GetFractionBackscattered() { return FractionBackscattered; }
double SRIM::GetFractionTransmitted() { return FractionTransmitted; }

#endif // SRIM_H
