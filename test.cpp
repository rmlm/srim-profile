// Compile with:
// g++ -o test test.cpp `root-config --cflags --glibs` -Wall

#include <cstdio>
#include <iostream>

#include <TApplication.h>
#include <TCanvas.h>
#include <TStyle.h>

#include "SRIM.h"

// Set ROOT asthetics
void SetROOTStyle() {
  gStyle->SetPadTickX(1);             // X-Axis Border Ticks
  gStyle->SetPadTickY(1);             // Y-Axis Border Ticks
  gStyle->SetPadGridX(0);             // X-Axis Major Gridlines
  gStyle->SetPadGridY(0);             // Y-Axis Major Gridlines
  gStyle->SetPadTopMargin(0.10);      // 0.1
  gStyle->SetPadBottomMargin(0.15);   // 0.1
  gStyle->SetPadLeftMargin(0.15);     // 0.1
  gStyle->SetPadRightMargin(0.15);    // 0.1
  gStyle->SetLabelFont(42, "xyz");    // Axes label fonts
  gStyle->SetTitleFont(42, "xyz");    // Axes title fonts
  gStyle->SetTitleOffset(1.0, "xyz"); // Axes title position offset
  gStyle->SetTitleSize(0.06, "xyz");  // Axes title size
  gStyle->SetTitleBorderSize(0);      // Border box around Histogram/Graph title
  gStyle->SetTitleFont(
      42, "1"); // Histogram/Graph title font - arg in "" must not be x, y, or z
  gStyle->SetTitleSize(0.06, "1");   // Axes title size
  gStyle->SetHistMinimumZero(kTRUE); // Zooms in on all points
  gStyle->SetStripDecimals(kFALSE);  // Keep Constant Decimals
  gStyle->SetOptStat(0);             // Options: ksiourmen 111111110
  gStyle->SetOptFit(0);              //
  gStyle->SetOptTitle(1);            // Display title
  gStyle->SetCanvasDefH(600);        // Canvas height (def. 500)
  gStyle->SetCanvasDefW(800);        // Canvas width (def. 700)
  gStyle->SetCanvasDefX(0);          // Canvas position on monitor
  gStyle->SetCanvasDefY(0);          // Canvas position on monitor
  // gStyle->SetPaperSize(21.6,27.9);  // Size in cm - US Letter Paper
  gStyle->SetPalette(55);     // Rainbow Palette... change this!
  gStyle->SetColorModelPS(0); // RGB=0, CMYK=1
  gStyle->SetLineScalePS(
      3); // Line scaling for Postscript output (3-5 seems OK)
}

int main(int argc, char **argv) {
  SetROOTStyle();
  TApplication theApp("theApp", &argc, argv);
  TCanvas canvas("canvas", "canvas", 800, 600);

  // SRIM srim("RANGE.txt");
  SRIM srim("RANGE.txt", "RANGE_3D.txt");
  srim.GetSummary();
  srim.SaveASCII("./");
  srim.hRANGE_3D_X.Draw("hist");

  /*
  std::cout << "GetVersion           = " << srim.GetVersion() << std::endl;
  std::cout << "GetIon               = " << srim.GetIon() << std::endl;
  std::cout << "GetIonsSimulated     = " << srim.GetIonsSimulated() <<
  std::endl; std::cout << "GetIonsImplanted     = " << srim.GetIonsImplanted()
  << std::endl; std::cout << "GetIonsTransmitted   = " <<
  srim.GetIonsTransmitted() << std::endl; std::cout << "GetIonsBackscattered = "
  << srim.GetIonsBackscattered() << std::endl; std::cout << "\n" << std::endl;

  std::cout << "---RANGE.txt---" << std::endl;
  std::cout << "GetRange             = " << srim.GetRange() << std::endl;
  std::cout << "GetStraggle          = " << srim.GetStraggle() << std::endl;
  std::cout << "GetLateralRange      = " << srim.GetLateralRange() << std::endl;
  std::cout << "GetLateralStraggle   = " << srim.GetLateralStraggle() <<
  std::endl; std::cout << "GetRadialRange       = " << srim.GetRadialRange() <<
  std::endl; std::cout << "GetRadialStraggle    = " << srim.GetRadialStraggle()
  << std::endl; std::cout << "\n" << std::endl;

  std::cout << "---RANGE_3D.txt---" << std::endl;
  std::cout << "GetRangeX            = " << srim.GetRangeX() << std::endl;
  std::cout << "GetStraggleX         = " << srim.GetStraggleX() << std::endl;
  std::cout << "GetRangeY            = " << srim.GetRangeY() << std::endl;
  std::cout << "GetStraggleY         = " << srim.GetStraggleY() << std::endl;
  std::cout << "GetRangeZ            = " << srim.GetRangeZ() << std::endl;
  std::cout << "GetStraggleZ         = " << srim.GetStraggleZ() << std::endl;
  std::cout << "\n" << std::endl;

  std::cout << "GetEnergy            = " << srim.GetEnergy() << std::endl;
  std::cout << "GetAngle             = " << srim.GetAngle() << std::endl;
  std::cout << "GetZ                 = " << srim.GetZ() << std::endl;
  std::cout << "GetMass              = " << srim.GetMass() << std::endl;
  */

  // srim.hRANGE.DrawCopy("hist");
  // TCanvas c1;
  // srim.gRANGE.Draw("AP");
  // c1.SaveAs("sd.pdf");
  // srim.hRANGE_3D_X.Fit(&srim.fGamma,"MER");
  // srim.hRANGE_3D_X.Fit(&srim.fBeta,"MER");

  // std::cout << srim.hRANGE_3D_X.Integral("width") << std::endl;
  // srim.hRANGE_3D_Y.DrawCopy("hist");
  // srim.hRANGE_3D_Z.DrawCopy("hist");
  // srim.hRANGE_3D.DrawCopy("hist");

  // This terminates the running TApplication when ANY canvas window is closed
  // (i.e., exited with "X") - call it before running the TApplication.
  // https://root.cern.ch/phpBB3/viewtopic.php?t=7183
  TQObject::Connect("TCanvas", "Closed()", "TApplication", gApplication,
                    "Terminate()");
  theApp.Run(kTRUE);
  return EXIT_SUCCESS;
}

void test() {
  SetROOTStyle();
  TCanvas *c1 = new TCanvas("c1", "c1", 800, 600);
  SRIM *srim_test = new SRIM("RANGE.txt", "RANGE_3D.txt");
  srim_test->GetSummary();
  srim_test->SaveASCII("./");
  srim_test->hRANGE_3D_X.Draw("hist");
  // srim_test->hRANGE_3D.Draw("BOX");
}
