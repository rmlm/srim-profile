// Example: Li+ implanted in beta-Sn
//
// Compile with:
// g++ -std=c++11 -o example example.cpp `root-config --cflags --glibs` -Wall

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glob.h>

#include <TApplication.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TQObject.h>
#include <TStyle.h>
#include <TColor.h>
#include <THStack.h>

#include "../SRIM.h"

// Set ROOT asthetics
void SetROOTStyle();
// Return directory contents containing "RANGE.txt" or "RANGE_3D.txt",
// eliminating the need to list every file by hand.
std::vector<std::string> glob_files(const std::string &pattern, bool range_3d);

int main(int argc, char *argv[])
{
   SetROOTStyle();
   TApplication theApp("theApp", &argc, argv);
   
   // Attempt to specify directory via the command line.
   /*
   //std::cout << "argc = " << argc << std::endl;
   //for (int i = 0; i <= argc-1; i++) std::cout << "argv[" << i << "] = " << argv[i] << std::endl;
   
   std::string path;
   // Get directory to look in from command line input.
   switch (argc)
   {
      case 1 :
         path.append("./data");
         break;
      case 2 :
         path.append(argv[argc-1]);
         break;
      default :
         std::cout << "Too many directories... exiting." << std::endl;
         return EXIT_FAILURE;
   }
   // Append the wildcard string.
   */
   //path.append("/*");
   
   std::string path("./data/*");
   
   // Vectors to hold the SRIM objects & other information we're interested in.
   std::vector<SRIM> srim_example;
   std::vector<double> energy;
   std::vector<double> range;
   std::vector<double> straggle;
   std::vector<double> backscattered;
   std::vector<double> transmitted;
   
   // Flag indicating if we want to read RANGE_3D outputs.
   bool Read_RANGE_3D = true;
   
   // Vectors to hold the filenames of the SRIM outputs.
   std::vector<std::string> srim_data_files = glob_files( path, false);
   std::vector<std::string> srim_data_files_3D = glob_files( path, true);
   
   // Print the names of the files found.
   /*
   std::cout << "\nRANGE.txt files:\n" << std::endl;
   for (auto & s : srim_data_files) std::cout << s << std::endl;
   std::cout << "\nRANGE_3D.txt files:\n" << std::endl;
   for (auto & s : srim_data_files_3D) std::cout << s << std::endl;
   */
   
   // Check if no SRIM outputs were found.
   if ( srim_data_files.size() == 0 )
   {
      std::cout << "No SRIM output files found in: " << path << std::endl;
      std::cout << "- files MUST contain 'RANGE.txt' or 'RANGE_3D.txt' in their filename." << std::endl;
      return EXIT_FAILURE;
   }
   
   // Loop over the files in the specified directory.
   if ( Read_RANGE_3D == true and srim_data_files.size() == srim_data_files_3D.size() )
   {
      for (std::size_t i = 0; i < srim_data_files.size(); ++i)
      {
         // Create the SRIM objects using RANGE.txt & RANGE_3D.txt outputs.
         srim_example.push_back( SRIM( srim_data_files.at(i), srim_data_files_3D.at(i) ) );
         // Get the information we're after.
         energy.push_back( srim_example.at(i).GetEnergy() );
         range.push_back( srim_example.at(i).GetRangeX() );
         straggle.push_back( srim_example.at(i).GetStraggleX() );
         backscattered.push_back ( srim_example.at(i).GetFractionBackscattered() );
         transmitted.push_back ( srim_example.at(i).GetFractionTransmitted() );
         // Save the implantation profiles.
         srim_example.at(i).SaveASCII("./output");
      }
   }
   else
   {
      for (std::size_t i = 0; i < srim_data_files.size(); ++i)
      {
         // Create the SRIM objects using RANGE.txt.
         srim_example.push_back( SRIM( srim_data_files.at(i) ) );
         // Get the information we're after.
         energy.push_back( srim_example.at(i).GetEnergy() );
         range.push_back( srim_example.at(i).GetRange() );
         straggle.push_back( srim_example.at(i).GetStraggle() );
         backscattered.push_back ( srim_example.at(i).GetFractionBackscattered() );
         transmitted.push_back ( srim_example.at(i).GetFractionTransmitted() );
         // Save the implantation profiles.
         srim_example.at(i).SaveASCII("./output");
      }
   }
   
   // Save a summary of the results to a text file.
   std::ofstream output_file;
   output_file.open ("./output/srim_results.dat");
   if ( output_file.is_open() )
   {
      output_file << "#Energy(keV)\tRange(nm)\tStraggle(nm)\tBackscattered\tTransmitted\n";
      for (std::size_t i = 0; i < energy.size(); ++i)
      {
         output_file << std::fixed << std::setprecision(9) << energy.at(i) << "\t" << range.at(i) << "\t" << straggle.at(i) << "\t" << backscattered.at(i) << "\t" << transmitted.at(i) << "\n";
      }
   }
   output_file.close();
   
   // Find the range of implantation energies.
   double energy_min = *std::min_element( energy.begin(), energy.end() );
   double energy_max = *std::max_element( energy.begin(), energy.end() );
   
   // Make some graphs, do some fitting, make some legends, etc...
   
   TGraph g_range(srim_example.size(), &energy.at(0), &range.at(0));
   g_range.SetTitle("Range;Implantation Energy (keV);Range (nm)");
   g_range.SetMarkerColor(kBlack);
   g_range.SetLineColor(kBlack);
   g_range.SetMarkerStyle(kFullCircle);
   
   TF1 f_range("f_range","[0]+[1]*x+[2]*x*x",energy_min,energy_max);
   f_range.SetLineColor(kRed);
   f_range.SetNpx(500);
   g_range.Fit("f_range","QMER");
   
   TGraph g_straggle(srim_example.size(), &energy.at(0), &straggle.at(0));
   g_straggle.SetTitle("Straggle;Implantation Energy (keV);Straggle (nm)");
   g_straggle.SetMarkerColor(kBlack);
   g_straggle.SetLineColor(kBlack);
   g_straggle.SetMarkerStyle(kCircle);
   
   TF1 f_straggle("f_straggle","[0]+[1]*x+[2]*x*x",energy_min,energy_max);
   f_straggle.SetLineColor(kBlue);
   f_straggle.SetNpx(500);
   g_straggle.Fit("f_straggle","QMER");
   
   TMultiGraph mg_range_straggle;
   mg_range_straggle.SetTitle(";Implantation Energy (keV);Depth (nm)");
   mg_range_straggle.Add(&g_range);
   mg_range_straggle.Add(&g_straggle);
   
   std::string s_range("#it{R} = ");
   s_range.append( std::to_string(f_range.GetParameter(0)) + " + " + std::to_string(f_range.GetParameter(1)) + "#it{E}  + " + std::to_string(f_range.GetParameter(2)) + "#it{E}^{2}");
   
   std::string s_straggle("#it{S} = ");
   s_straggle.append( std::to_string(f_straggle.GetParameter(0)) + " + " + std::to_string(f_straggle.GetParameter(1)) + "#it{E}  + " + std::to_string(f_straggle.GetParameter(2)) + "#it{E}^{2}");
   
   TLegend l_range_straggle(0.20,0.70,0.60,0.85);
   l_range_straggle.AddEntry(&g_range, "Range","p");
   l_range_straggle.AddEntry(&f_range, s_range.c_str(),"l");
   l_range_straggle.AddEntry(&g_straggle, "Straggle","p");
   l_range_straggle.AddEntry(&f_straggle, s_straggle.c_str(),"l");
   
   TGraph g_backscattered(srim_example.size(), &energy.at(0), &backscattered.at(0));
   g_backscattered.SetTitle(";Implantation Energy (keV);Fraction Backscattered");
   g_backscattered.SetMarkerColor(kBlack);
   g_backscattered.SetLineColor(kBlack);
   g_backscattered.SetMarkerStyle(kFullCircle);
   
   TGraph g_transmitted(srim_example.size(), &energy.at(0), &transmitted.at(0));
   g_transmitted.SetTitle("Straggle;Implantation Energy (keV);Fraction Transmitted");
   g_transmitted.SetMarkerColor(kBlack);
   g_transmitted.SetLineColor(kBlack);
   g_transmitted.SetMarkerStyle(kCircle);
   
   TMultiGraph mg_backscattered_transmitted;
   mg_backscattered_transmitted.SetTitle(";Implantation Energy (keV);Fraction");
   mg_backscattered_transmitted.Add(&g_backscattered);
   mg_backscattered_transmitted.Add(&g_transmitted);
   
   TLegend l_backscattered_transmitted(0.55,0.75,0.80,0.85);
   l_backscattered_transmitted.AddEntry(&g_backscattered, "Backscattered","p");
   l_backscattered_transmitted.AddEntry(&g_transmitted, "Transmitted","p");
   
   TLegend l_profile(0.55,0.75,0.80,0.85);
   
   TMultiGraph mg_profile;
   mg_profile.SetTitle(";Depth (nm);Stopping Probability");
   
   THStack hs_profile;
   hs_profile.SetTitle(";Depth (nm);Stopping Probability (nm^{-1})");
   
   for (std::size_t i = 0; i < srim_example.size(); ++i)
   {
      // Colours in the range of [0,1]
      float red = 0.0;
      float green = 1.0 - static_cast<float>(i)/(static_cast<float>(srim_example.size())-1.0);
      float blue = static_cast<float>(i)/(static_cast<float>(srim_example.size())-1.0);
      srim_example.at(i).gRANGE.SetMarkerColor( TColor::GetColor(red, green, blue) );
      srim_example.at(i).gRANGE.SetLineColor( TColor::GetColor(red, green, blue) );
      std::string energy_label;
      energy_label.append( std::to_string(energy.at(i)) + " keV");
      l_profile.AddEntry(&srim_example.at(i).gRANGE, energy_label.c_str(),"p");
      mg_profile.Add(&srim_example.at(i).gRANGE);
      
      if ( Read_RANGE_3D == true and srim_data_files.size() == srim_data_files_3D.size() )
      {
         srim_example.at(i).hRANGE_3D_X.SetMarkerColor( TColor::GetColor(red, green, blue) );
         srim_example.at(i).hRANGE_3D_X.SetLineColor( TColor::GetColor(red, green, blue) );
         hs_profile.Add(&srim_example.at(i).hRANGE_3D_X);
      }
   }
   
   // Make some canvases and draw the graphs.
   
   TCanvas c_backscattered_transmitted("c_backscattered_transmitted","Fraction Backscattered & Transmitted",gStyle->GetCanvasDefW(),gStyle->GetCanvasDefH());
   mg_backscattered_transmitted.Draw("AP");
   //mg_backscattered_transmitted.GetXaxis()->CenterTitle();
   //mg_backscattered_transmitted.GetYaxis()->CenterTitle();
   l_backscattered_transmitted.Draw();
   
   TCanvas c_range_straggle("c_range_straggle","Range & Straggle",gStyle->GetCanvasDefW(),gStyle->GetCanvasDefH());
   mg_range_straggle.Draw("AP");
   //mg_range_straggle.GetXaxis()->CenterTitle();
   //mg_range_straggle.GetYaxis()->CenterTitle();
   l_range_straggle.Draw();
   
   TCanvas c_profile("c_profile","RANGE.txt Stopping Profiles",gStyle->GetCanvasDefW(),gStyle->GetCanvasDefH());
   mg_profile.Draw("AP");
   //mg_profile.GetXaxis()->CenterTitle();
   //mg_profile.GetYaxis()->CenterTitle();
   l_profile.Draw();
   
   TCanvas c_profile_3D("c_profile_3D","RANGE_3D.txt Stopping Profiles",gStyle->GetCanvasDefW(),gStyle->GetCanvasDefH());
   if ( Read_RANGE_3D == true and srim_data_files.size() == srim_data_files_3D.size() )
   {
      hs_profile.Draw("nostack");
      l_profile.Draw();
   }
   else
   {
      c_profile_3D.~TCanvas();
   }
   
   // This terminates the running TApplication when ANY canvas window is closed
   // (i.e., exited with "X") - call it before running the TApplication.
   // https://root.cern.ch/phpBB3/viewtopic.php?t=7183
   TQObject::Connect("TCanvas", "Closed()", "TApplication", gApplication, "Terminate()");
   theApp.Run(kTRUE);
   return EXIT_SUCCESS;
}


// Set ROOT asthetics
void SetROOTStyle()
{
   gStyle->SetPadTickX(1);             // X-Axis Border Ticks
   gStyle->SetPadTickY(1);             // Y-Axis Border Ticks
   gStyle->SetPadGridX(0);             // X-Axis Major Gridlines
   gStyle->SetPadGridY(0);             // Y-Axis Major Gridlines
   gStyle->SetPadTopMargin(0.10);		// 0.1
   gStyle->SetPadBottomMargin(0.15);	// 0.1
   gStyle->SetPadLeftMargin(0.15);		// 0.1
   gStyle->SetPadRightMargin(0.15);	   // 0.1
   gStyle->SetLabelFont(42,"xyz");		// Axes label fonts
   gStyle->SetTitleFont(42,"xyz");		// Axes title fonts
   gStyle->SetTitleOffset(1.0,"xyz");	// Axes title position offset
   gStyle->SetTitleSize(0.06,"xyz"); 	// Axes title size
   gStyle->SetTitleBorderSize(0);		// Border box around Histogram/Graph title
   gStyle->SetTitleFont(42,"1");		   // Histogram/Graph title font - arg in "" must not be x, y, or z
   gStyle->SetTitleSize(0.06,"1"); 	   // Axes title size
   gStyle->SetHistMinimumZero(kTRUE);	// Zooms in on all points
   gStyle->SetStripDecimals(kFALSE);   // Keep Constant Decimals
   gStyle->SetOptStat(0);              // Options: ksiourmen 111111110
   gStyle->SetOptFit(0);               // 
   gStyle->SetOptTitle(1);	            // Display title
   
   // Get the resolution of the current display and use it to determine the size of the canvas.
   // http://superuser.com/a/338697
   std::string get_width = "xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1";
   std::string get_height = "xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2";
   double screen_height;
   FILE *pin = popen(get_height.c_str(), "r");
	char buff[2048];
	if ( pin != 0 )
	{
      fgets(buff, sizeof(buff), pin);
      screen_height = std::atof(buff);
   }
   else
   {
      screen_height = 1080;
   }
   pclose(pin);
   
   if ( screen_height <= 0 )
   {
      screen_height = 1080;
   }
   // Canvas dimensions (4:3)
   double aspect_ratio = 4.0/3.0;
   double screen_fraction = 0.6;
   double height = screen_height*screen_fraction;
   double width = height*aspect_ratio;
   
   gStyle->SetCanvasDefH(height);      // Canvas height (def. 500)	
   gStyle->SetCanvasDefW(width);       // Canvas width (def. 700)
   gStyle->SetCanvasDefX(0);           // Canvas position on monitor
   gStyle->SetCanvasDefY(0);           // Canvas position on monitor
   //gStyle->SetPaperSize(21.6,27.9);  // Size in cm - US Letter Paper
   gStyle->SetPalette(55);             // Rainbow Palette... change this!
   gStyle->SetColorModelPS(0);         // RGB=0, CMYK=1
   gStyle->SetLineScalePS(3);          // Line scaling for Postscript output (3-5 seems OK)
}


// Return directory contents containing "RANGE.txt" or "RANGE_3D.txt",
// eliminating the need to list every file by hand.
// http://stackoverflow.com/a/24703135
std::vector<std::string> glob_files(const std::string &pattern, bool range_3d)
{
   glob_t glob_result;
   glob( pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
    std::vector<std::string> files;
    for (unsigned int i = 0 ; i < glob_result.gl_pathc; ++i)
    {
      std::string glob_out( glob_result.gl_pathv[i] );
      if ( range_3d == false and glob_out.find("RANGE.txt") != std::string::npos )
      {
         files.push_back( glob_out );
      }
      if ( range_3d == true and glob_out.find("RANGE_3D.txt") != std::string::npos )
      {
         files.push_back( glob_out );
      }
    }
    globfree(&glob_result);
    return files;
}
