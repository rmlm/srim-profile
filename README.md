# SRIM Profile Class

### About

This is a C++ class for reading the output files from [SRIM] calculations. Its primary goal is to provide a convenient interface for creating ion-implantation profiles to be used in the analysis of [β-NMR] experiments at [TRIUMF].

### Requirements

- C++11
- [ROOT] v6


### Use

Currently, the class is implemented as a header only (i.e., no shared library); however, it can be easily integrated in scripts run through [ROOT]'s interactive shell. The provided `test.cpp` script can be run using, e.g.:

```sh
$ root -l test.cpp
```

Alternatively, it can be compiled into a standalone application with, e.g.:

```sh
$ g++ -o test test.cpp `root-config --cflags --glibs` -Wall
```

### To-do list

- Add continuous PDFs and automatically set them to approximate the (discrete) implantation profiles.
- Split SRIM.h into SRIM.h and SRIM.cpp.
- Add makefile for building shared library.


[SRIM]: <http://srim.org/>
[ROOT]: <https://root.cern.ch/>
[TRIUMF]: <http://www.triumf.ca/>
[β-NMR]: <http://bnmr.triumf.ca/>
